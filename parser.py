from rply import ParserGenerator, ParsingError
from ast import Number, Sum, BinaryOperation, Sub, Mul, Div, If_Else, Statements, Declare, Var, Cond, Loop, Assign, Return
import functools as f
import operator
import logging
import pprint

class Parser():
    def __init__(self, module, builder):
        self.pg = ParserGenerator(
            [
                'NUMBER', 
                'OPEN_PAREN',
                'CLOSE_PAREN', 
                'SUB',
                'SUM',
                'MUL',
                'DIV',
                'IF',
                'TRUE',
                'FALSE',
                'OPEN_CURLY_BRA',
                'CLOSE_CURLY_BRA',
                'DEF',
                'VAR',
                'LOOP',
                'LE',
                'LTE',
                'GE',
                'GTE',
                'EQ',
                'ASSIGN',
                'RETURN'
            ],
            precedence = [
                # TODO: Fix precedence - before it was 'left' instead of right but that doesn't
                # seem to work either
                ('right', ['SUM', 'SUB']),
                ('right', ['MULTIPLY'])
            ]
        )

        self.module = module
        self.builder = builder
        self.symbol_table = {}
        self.state = {'depth': 0}

    def parse(self):
        @self.pg.production('program : OPEN_CURLY_BRA statements return CLOSE_CURLY_BRA')
        def program(p):
            # REVIEW
            pprint.pprint(p[1].__dict__)
            pprint.pprint(p[2])
            print("\n\n")
            p[1].eval()
            p[2].eval()
        
        @self.pg.production('block : OPEN_CURLY_BRA statements CLOSE_CURLY_BRA')
        def block(p):
            return p[1]
            
        @self.pg.production('statements : ')
        def empty_expression(p):
            return Statements(self.state)     

        @self.pg.production('statements : statements expression')
        @self.pg.production('statements : statements declaration')
        @self.pg.production('statements : statements assignment')
        @self.pg.production('statements : statements loop')
        @self.pg.production('statements : statements return')
        def statements(p):
            # TODO: https://stackoverflow.com/questions/55532875/python-rply-reverse-parser
            p[0].add(p[1])
            return p[0]
        
        @self.pg.production('expression : OPEN_PAREN expression CLOSE_PAREN')
        def expression(p):
            """
            Make it as generic as possible, the operator is in charge of
            returning the Class to be applied to the list
            """
            return p[1]

        @self.pg.production('expression : OPEN_PAREN IF cond-expression block block CLOSE_PAREN')
        def conditional(p):
            return If_Else(self.builder, self.module, p[2], p[3], p[4], self.state)
            

        @self.pg.production('cond-expression : OPEN_PAREN expression conditional-op expression CLOSE_PAREN')
        def cond_expression(p):
            return Cond(self.builder, self.module, p[2], p[1], p[3], self.state)

        
        @self.pg.production('conditional-op : LTE')
        @self.pg.production('conditional-op : GTE')
        @self.pg.production('conditional-op : GE')
        @self.pg.production('conditional-op : EQ')
        def cond_op(p):
            return p[0].value

        @self.pg.production('declaration : DEF VAR expression')
        def declare(p):
            return Declare(self.builder, self.module, p[1], p[2], self.symbol_table, self.state)

        @self.pg.production('expression : cond-expression')
        def expression_cond(p):
            return p[0]

        @self.pg.production('expression : NUMBER')
        def operation_number(p):
            return Number(int(p[0].value), self.state)

        @self.pg.production('expression : expression operator expression')
        def bin_expression(p):
            return p[1](self.builder, self.module, p[0], p[2], self.state)
        
        @self.pg.production('expression : VAR')
        @self.pg.production('var : VAR')
        def var(p):
            return Var(self.builder, self.module, p[0], self.symbol_table, self.state)

        @self.pg.production('operator : SUM')
        @self.pg.production('operator : SUB')
        @self.pg.production('operator : MUL')
        @self.pg.production('operator : DIV')
        def op(p):
            operator = p[0]
            if operator.gettokentype() == 'SUM':
                return Sum
            elif operator.gettokentype() == 'SUB':
                return Sub
            elif operator.gettokentype() == 'MUL':
                return Mul
            elif operator.gettokentype() == "DIV":
                return Div

        @self.pg.production('loop : OPEN_PAREN LOOP cond-expression block CLOSE_PAREN')
        def loop(p):
            return Loop(self.builder, self.module, p[2], p[3], self.state)

        @self.pg.production('return : RETURN expression')
        def ret(p):
            return Return(self.builder, self.module, p[1], self.state)

        @self.pg.production('assignment : var ASSIGN expression')
        def assignment(p):
            return Assign(self.builder, self.module, p[0], p[2], self.symbol_table, self.state)
        
        @self.pg.error
        def error_handler(token):
            raise Exception(f"Ran into a {token.gettokentype()} where it was't expected \n{token.source_pos}")

    def get_parser(self):
        return self.pg.build()