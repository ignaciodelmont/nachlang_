from lexer import Lexer
from parser import Parser
from codegen import CodeGen
import logging, pprint
from ctypes import CFUNCTYPE, c_int32

logging.basicConfig(level= logging.DEBUG)
lexer = Lexer().get_lexer()

codegen = CodeGen()

module = codegen.module
builder = codegen.builder
engine = codegen.engine

file_base = input("input file base name: ")
filename = f"{file_base}.nach"
f = open(filename, "r")
program = f.read()
tokens = lexer.lex(program)

pg = Parser(module, builder)
pg.parse()
parser = pg.get_parser()
print(f"\n\n\n\n{'-'*20}AST{'-'*20}")
parser.parse(tokens)

print(f"\n\n\n\n{'-'*20}SYMBOL TABLE{'-'*20}\n")
pprint.pprint(pg.symbol_table)
print(f"\n\n\n\n{'-'*20}PRODUCTIONS{'-'*20}\n")
pprint.pprint(pg.pg.__dict__['productions'])

print(f"\n\n\n\n{'-'*20}CODEGEN{'-'*20}")
# CODEGEN
mod = codegen.create_ir()
print(mod)
codegen.save_ir(f"{file_base}.llvm")
# Look up the function pointer (a Python int)
func_ptr = engine.get_function_address("main")
print("function pointer:", func_ptr)
c_fun = CFUNCTYPE(c_int32)(func_ptr)
print(f"\n\n\n\n{'-'*20}EXECUTION RESULTS{'-'*20}")
print(f'\n RESULT: ', c_fun())


