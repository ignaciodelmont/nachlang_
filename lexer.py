from rply import ParserGenerator, LexerGenerator
from rply.token import BaseBox

class Lexer():
    def __init__(self):
        self.lexer = LexerGenerator()

    def _add_tokens(self):
        # Print
        self.lexer.add('PRINT', r'print')

        # Conditional
        self.lexer.add('IF', r'if')
        
        # Function
        self.lexer.add("LOOP", r"loop")
        self.lexer.add("RETURN", r"return")

        # Booleans
        self.lexer.add('TRUE', r'true')
        self.lexer.add('FALSE', r'false')

        # Parenthesis
        self.lexer.add('OPEN_PAREN', r'\(')
        self.lexer.add('CLOSE_PAREN', r'\)')

        # Curly brackets
        self.lexer.add('OPEN_CURLY_BRA', r'\{')
        self.lexer.add('CLOSE_CURLY_BRA', r'\}')

        # Operators
        self.lexer.add('SUM', r'\+')
        self.lexer.add('SUB', r'\-')
        self.lexer.add('MUL', r'\*')
        self.lexer.add('DIV', r'\/')

        # Conditional Operators
        self.lexer.add('LTE', r'<=')
        self.lexer.add('GTE', r'>=')
        self.lexer.add('EQ', r'==')
        self.lexer.add('LT', r'<')
        self.lexer.add('GT', r'>')

        # Vars
        self.lexer.add("DEF", r"def")
        self.lexer.add("VAR", r"[a-zA-Z_][a-zA-Z0-9_]*")
        self.lexer.add("ASSIGN", r"=")


        # Number
        self.lexer.add('NUMBER', r'\d+')

        # Ignore spaces and new lines
        self.lexer.ignore(r' |\n')
        # Ignore comments
        self.lexer.ignore(r'\#.*')


    def get_lexer(self):
        self._add_tokens()
        return self.lexer.build()