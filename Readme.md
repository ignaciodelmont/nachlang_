# Final Teoría de Lenguajes

## Universidad Católica Argentina
## Facultad de ingeniería y ciencias agrarias

### Alumno: Ignacio Delmont
### Profesores: Javier Ouret, Ignacio Parravicini

---

## Objetivo del trabajo
### Realizar un compilador para un lenguaje de programación sencillo. Éste compilador está compuesto por un Lexer y un Parser que permíten generar el árbol sintáctico. Para estos se utilizó [rply](https://rply.readthedocs.io/en/latest/). También está compuesto por un generador de código, que genera código para [llvm](https://llvm.org/docs/GettingStarted.html), para lo cual se utilizó [llvmlite](https://llvmlite.readthedocs.io/en/latest/).
---
## Uso
### En la carpeta examples hay varios ejemplos con la extensión `.nach`. Los archivos pueden compilarse y ejecutarse corriendo el script `compile.py`. Al correrse, se verá como output el AST para el código, las reglas de producción del Parser, el output del codegenerator y el resultado de la ejecución.
---
## Estructura
- `lexer.py` : Como dice el nombre es el lexer. En este archivo se encuentran las definiciones los tokens y las reglas de carácteres a ignorar.
- `parser.py` : Es el parser. En este archivo se encuentran las reglas de producción con los terminales y no terminales. Acá se inicializan el builder y el módulo del codegenerator que son pasados a la capa inferior `ast.py`. También se declara la `symbol table` y el `state` para llevar trackeo del estado del `depth` del árbol de ejecución.
- `ast.py` : Se encarga de evaluar los terminales y no terminales a partir de lo parseado. Hace el `build` de las IRs de llvm a partir de las evaluaciones.
---
## Ejemplo de ejecución `fib.nach`
### OUTPUT:
```

--------------------AST--------------------
{'state': {'depth': 0},
 'statements': [<ast.Declare object at 0x7ffdc421ddf0>,
                <ast.Declare object at 0x7ffdc4226a60>,
                <ast.Declare object at 0x7ffdc4226a30>,
                <ast.Declare object at 0x7ffdc4226850>,
                <ast.Declare object at 0x7ffdc4226940>,
                <ast.Loop object at 0x7ffdc42436a0>]}
<ast.Return object at 0x7ffdc42262e0>



STATEMENTS [<ast.Declare object at 0x7ffdc421ddf0>, <ast.Declare object at 0x7ffdc4226a60>, <ast.Declare object at 0x7ffdc4226a30>, <ast.Declare object at 0x7ffdc4226850>, <ast.Declare object at 0x7ffdc4226940>, <ast.Loop object at 0x7ffdc42436a0>]
    DECLARE fib_1 <ast.Number object at 0x7ffdc421df40>
        NUMBER 1
    DECLARE fib_2 <ast.Number object at 0x7ffdc421de20>
        NUMBER 1
    DECLARE fib_n <ast.Number object at 0x7ffdc421dcd0>
        NUMBER 5
    DECLARE i <ast.Sub object at 0x7ffdc4226a90>
        SUB <ast.Var object at 0x7ffdc4226910> <ast.Number object at 0x7ffdc42266d0>
            VAR fib_n
            NUMBER 2
    DECLARE result <ast.Var object at 0x7ffdc4226880>
        VAR fib_1
    LOOP <ast.Cond object at 0x7ffdc42263a0> <ast.Statements object at 0x7ffdc42264c0>
        COND >= <ast.Var object at 0x7ffdc4226580> <ast.Number object at 0x7ffdc4226340>
            VAR i
            NUMBER 1
        STATEMENTS [<ast.Assign object at 0x7ffdc4226070>, <ast.Assign object at 0x7ffdc4226fa0>, <ast.Assign object at 0x7ffdc4243250>, <ast.Assign object at 0x7ffdc4243490>]
            ASSIGN <ast.Var object at 0x7ffdc4226310> <ast.Sum object at 0x7ffdc4226d90>
                SUM <ast.Var object at 0x7ffdc4226130> <ast.Var object at 0x7ffdc4226d60>
                    VAR fib_1
                    VAR fib_2
            ASSIGN <ast.Var object at 0x7ffdc4226190> <ast.Var object at 0x7ffdc4226dc0>
                VAR fib_2
            ASSIGN <ast.Var object at 0x7ffdc4226e20> <ast.Var object at 0x7ffdc42261c0>
                VAR result
            ASSIGN <ast.Var object at 0x7ffdc42260a0> <ast.Sub object at 0x7ffdc42433a0>
                SUB <ast.Var object at 0x7ffdc4226fd0> <ast.Number object at 0x7ffdc4243670>
                    VAR i
                    NUMBER 1
        COND >= <ast.Var object at 0x7ffdc4226580> <ast.Number object at 0x7ffdc4226340>
            VAR i
            NUMBER 1
RETURN <ast.Var object at 0x7ffdc4226520>
    VAR result




--------------------SYMBOL TABLE--------------------

{'fib_1': <ir.AllocaInstr '.2' of type 'i32*', opname 'alloca', operands ()>,
 'fib_2': <ir.AllocaInstr '.4' of type 'i32*', opname 'alloca', operands ()>,
 'fib_n': <ir.AllocaInstr '.6' of type 'i32*', opname 'alloca', operands ()>,
 'i': <ir.AllocaInstr '.8' of type 'i32*', opname 'alloca', operands ()>,
 'result': <ir.AllocaInstr '.12' of type 'i32*', opname 'alloca', operands ()>}




--------------------PRODUCTIONS--------------------

[('program',
  ['OPEN_CURLY_BRA', 'statements', 'return', 'CLOSE_CURLY_BRA'],
  <function Parser.parse.<locals>.program at 0x7ffdc4205310>,
  None),
 ('block',
  ['OPEN_CURLY_BRA', 'statements', 'CLOSE_CURLY_BRA'],
  <function Parser.parse.<locals>.block at 0x7ffdc42053a0>,
  None),
 ('statements',
  [],
  <function Parser.parse.<locals>.empty_expression at 0x7ffdc4205430>,
  None),
 ('statements',
  ['statements', 'return'],
  <function Parser.parse.<locals>.statements at 0x7ffdc4205700>,
  None),
 ('statements',
  ['statements', 'loop'],
  <function Parser.parse.<locals>.statements at 0x7ffdc4205700>,
  None),
 ('statements',
  ['statements', 'assignment'],
  <function Parser.parse.<locals>.statements at 0x7ffdc4205700>,
  None),
 ('statements',
  ['statements', 'declaration'],
  <function Parser.parse.<locals>.statements at 0x7ffdc4205700>,
  None),
 ('statements',
  ['statements', 'expression'],
  <function Parser.parse.<locals>.statements at 0x7ffdc4205700>,
  None),
 ('expression',
  ['OPEN_PAREN', 'expression', 'CLOSE_PAREN'],
  <function Parser.parse.<locals>.expression at 0x7ffdc42054c0>,
  None),
 ('expression',
  ['OPEN_PAREN', 'IF', 'cond-expression', 'block', 'block', 'CLOSE_PAREN'],
  <function Parser.parse.<locals>.conditional at 0x7ffdc4205550>,
  None),
 ('cond-expression',
  ['OPEN_PAREN', 'expression', 'conditional-op', 'expression', 'CLOSE_PAREN'],
  <function Parser.parse.<locals>.cond_expression at 0x7ffdc42055e0>,
  None),
 ('conditional-op',
  ['EQ'],
  <function Parser.parse.<locals>.cond_op at 0x7ffdc42058b0>,
  None),
 ('conditional-op',
  ['GE'],
  <function Parser.parse.<locals>.cond_op at 0x7ffdc42058b0>,
  None),
 ('conditional-op',
  ['GTE'],
  <function Parser.parse.<locals>.cond_op at 0x7ffdc42058b0>,
  None),
 ('conditional-op',
  ['LTE'],
  <function Parser.parse.<locals>.cond_op at 0x7ffdc42058b0>,
  None),
 ('declaration',
  ['DEF', 'VAR', 'expression'],
  <function Parser.parse.<locals>.declare at 0x7ffdc4205670>,
  None),
 ('expression',
  ['cond-expression'],
  <function Parser.parse.<locals>.expression_cond at 0x7ffdc4205790>,
  None),
 ('expression',
  ['NUMBER'],
  <function Parser.parse.<locals>.operation_number at 0x7ffdc4205820>,
  None),
 ('expression',
  ['expression', 'operator', 'expression'],
  <function Parser.parse.<locals>.bin_expression at 0x7ffdc4205940>,
  None),
 ('var', ['VAR'], <function Parser.parse.<locals>.var at 0x7ffdc4205a60>, None),
 ('expression',
  ['VAR'],
  <function Parser.parse.<locals>.var at 0x7ffdc4205a60>,
  None),
 ('operator',
  ['DIV'],
  <function Parser.parse.<locals>.op at 0x7ffdc4205c10>,
  None),
 ('operator',
  ['MUL'],
  <function Parser.parse.<locals>.op at 0x7ffdc4205c10>,
  None),
 ('operator',
  ['SUB'],
  <function Parser.parse.<locals>.op at 0x7ffdc4205c10>,
  None),
 ('operator',
  ['SUM'],
  <function Parser.parse.<locals>.op at 0x7ffdc4205c10>,
  None),
 ('loop',
  ['OPEN_PAREN', 'LOOP', 'cond-expression', 'block', 'CLOSE_PAREN'],
  <function Parser.parse.<locals>.loop at 0x7ffdc42059d0>,
  None),
 ('return',
  ['RETURN', 'expression'],
  <function Parser.parse.<locals>.ret at 0x7ffdc4205af0>,
  None),
 ('assignment',
  ['var', 'ASSIGN', 'expression'],
  <function Parser.parse.<locals>.assignment at 0x7ffdc4205b80>,
  None)]




--------------------CODEGEN--------------------
; ModuleID = '<string>'
source_filename = "<string>"
target datalayout = "e-m:o-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-darwin20.1.0"

define i32 @main() {
entry:
  %.2 = alloca i32
  store i32 1, i32* %.2
  %.4 = alloca i32
  store i32 1, i32* %.4
  %.6 = alloca i32
  store i32 5, i32* %.6
  %.8 = alloca i32
  %.9 = load i32, i32* %.6
  %.10 = sub i32 %.9, 2
  store i32 %.10, i32* %.8
  %.12 = alloca i32
  %.13 = load i32, i32* %.2
  store i32 %.13, i32* %.12
  %.15 = load i32, i32* %.8
  %.16 = icmp sge i32 %.15, 1
  br i1 %.16, label %loop_body.preheader, label %loop_after

loop_body.preheader:                              ; preds = %entry
  br label %loop_body

loop_body:                                        ; preds = %loop_body.preheader, %loop_body
  %.18 = load i32, i32* %.2
  %.19 = load i32, i32* %.4
  %.20 = add i32 %.18, %.19
  store i32 %.20, i32* %.12
  %.22 = load i32, i32* %.4
  store i32 %.22, i32* %.2
  %.24 = load i32, i32* %.12
  store i32 %.24, i32* %.4
  %.26 = load i32, i32* %.8
  %.27 = sub i32 %.26, 1
  store i32 %.27, i32* %.8
  %.29 = load i32, i32* %.8
  %.30 = icmp sge i32 %.29, 1
  br i1 %.30, label %loop_body, label %loop_after

loop_after:                                       ; preds = %loop_body, %entry
  %.32 = load i32, i32* %.12
  ret i32 %.32
}

function pointer: 4470419472




--------------------EXECUTION RESULTS--------------------

 RESULT:  5
```