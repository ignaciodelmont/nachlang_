import functools as f
import logging
from llvmlite import ir

INT32 = ir.IntType(32)
INT1 = ir.IntType(1)

class Number():
    def __init__(self, value, state):
        self.value = value
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}NUMBER", self.value)
        i = ir.Constant(INT32, int(self.value))
        return i

class BinaryOperation():
    """
    Binary ops should return Numbers
    """

    def __init__(self, builder, module, left, right, state):
        self.builder = builder
        self.module = module
        self.left = left
        self.right = right
        self.state = state

class Sum(BinaryOperation):
    """
    Binary addition
    """
    def eval(self):
        print(f"{'    ' * self.state['depth']}SUM", self.left, self.right)
        self.state['depth']+=1
        left = self.left.eval()
        right = self.right.eval()
        self.state['depth']-=1
        return self.builder.add(left, right)

class Sub(BinaryOperation):
    """
    Binary substraction
    """
    def eval(self):
        print(f"{'    ' * self.state['depth']}SUB", self.left, self.right)
        self.state['depth']+=1
        left = self.left.eval()
        right = self.right.eval()
        self.state['depth']-=1
        return self.builder.sub(left, right)

class Mul(BinaryOperation):
    """
    Binary substraction
    """
    def eval(self):
        print(f"{'    ' * self.state['depth']}MUL", self.left, self.right)
        self.state['depth']+=1
        left = self.left.eval()
        right = self.right.eval()
        self.state['depth']-=1
        return self.builder.mul(left, right)

class Div(BinaryOperation):
    """
    Binary division
    """
    # TODO: see if div by 0 can be handled
    def eval(self):
        print(f"{'    ' * self.state['depth']}DIV", self.left, self.right)
        self.state['depth']+=1
        left = self.left.eval()
        right = self.right.eval()
        self.state['depth']-=1
        return self.builder.sdiv(left, right)
 
class If_Else():
    def __init__(self, builder, module, condition, left, right, state):
        self.builder = builder
        self.module = module
        self.condition = condition
        self.left = left
        self.right = right
        self.state =  state

    def eval(self):
        print(f"{'    ' * self.state['depth']}IF ELSE", self.condition, self.left, self.right)
        self.state['depth'] += 1
        with self.builder.if_else(self.condition.eval()) as (then, otherwise):
            with then:
                self.left.eval()
            with otherwise:
                self.right.eval()
        self.state['depth'] -= 1

class Statements():
    def __init__(self, state, statement=None):
        a = [] if not statement else [statement]
        self.statements = a
        self.state = state
        
    def add(self, statement):
        self.statements.append(statement)

    def eval(self):
        print(f"{'    ' * self.state['depth']}STATEMENTS", self.statements)
        self.state['depth']+=1
        for statement in self.statements:
            statement.eval()
        self.state['depth']-=1

class Declare():
    def __init__(self, builder, module, var_name, expression, symbol_table, state):
        self.builder = builder
        self.module = module
        self.value = expression
        self.var_name = var_name.value
        self.symbol_table = symbol_table
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}DECLARE", self.var_name, self.value)
        self.state['depth'] += 1
        var_pointer = self.builder.alloca(INT32)
        self.symbol_table[self.var_name] = var_pointer
        self.builder.store(self.value.eval(), var_pointer)
        self.state['depth'] -= 1

class Var():
    def __init__(self, builder, module, var, symbol_table, state):
        self.builder = builder
        self.module = module
        self.var = var
        self.symbol_table = symbol_table
        self.state = state

    def eval(self):
        var_name = self.var.value
        print(f"{'    ' * self.state['depth']}VAR", var_name)
        pointer = self.symbol_table.get(var_name, None)
        if not pointer:
            raise Exception(f"Undeclared var '{var_name}'|\nLine: {self.var.source_pos}")
        value = self.builder.load(pointer)
        return value

class Cond():
    def __init__(self, builder, module, cond_op, left, right, state):
        self.builder = builder
        self.module = module
        self.cond_op = cond_op
        self.left = left
        self.right = right
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}COND", self.cond_op, self.left, self.right)
        self.state['depth'] += 1
        i = self.builder.icmp_signed(self.cond_op, self.left.eval(), self.right.eval())
        self.state['depth'] -= 1
        return i

class Loop():
    def __init__(self, builder, module, cond_expression, block, state):
        self.builder = builder
        self.module = module
        self.cond_expression = cond_expression
        self.block = block
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}LOOP", self.cond_expression, self.block)
        loop_body_block = self.builder.append_basic_block("loop_body")
        loop_after_block = self.builder.append_basic_block("loop_after")
        self.state['depth'] += 1
        self.builder.cbranch(self.cond_expression.eval(), loop_body_block, loop_after_block)
        self.builder.position_at_start(loop_body_block)
        self.block.eval()
        self.builder.cbranch(self.cond_expression.eval(), loop_body_block, loop_after_block)
        self.builder.position_at_start(loop_after_block)
        self.state['depth'] -= 1

class Assign():
    def __init__(self, builder, module, var, value, symbol_table, state):
        self.builder = builder
        self.module = module
        self.var = var
        self.value = value
        self.symbol_table = symbol_table
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}ASSIGN", self.var, self.value)
        var_name = self.var.var.value
        var_pointer = self.symbol_table.get(var_name, None)
        if not var_pointer:
            raise Exception(f"Undeclared variable {var_name}")
        
        self.state['depth'] += 1
        self.builder.store(self.value.eval(), var_pointer)
        self.state['depth'] -= 1

class Return():
    def __init__(self, builder, module, expression, state):
        self.builder = builder
        self.module = module
        self.expression = expression
        self.state = state

    def eval(self):
        print(f"{'    ' * self.state['depth']}RETURN", self.expression)
        self.state['depth'] += 1
        i = self.builder.ret(self.expression.eval())
        self.state['depth'] -= 1
        return i